FROM python:3.11

COPY ./requirements.txt /app/requirements.txt
COPY ./app /app/app
COPY ./tests /app/tests

WORKDIR /app

RUN pip install -r requirements.txt

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8000"]