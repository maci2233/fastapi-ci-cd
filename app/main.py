from typing import Any
from typing import Union

from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()

fake_items_db = [{"item_name": "Foo"}, {"item_name": "Bar"}, {"item_name": "Baz"}]


class Item(BaseModel):
    name: str
    description: Union[str, None] = None
    price: float
    tax: Union[float, None] = None


class ItemOut(BaseModel):
    name: str
    description: Union[str, None] = None
    price: float


@app.get("/")
async def root():
    return {
        "message": "Hello World again my brothers!"
    }


@app.get("/items")
async def get_items(skip: int = 0, limit: int = 10):
    return fake_items_db[skip : skip + limit]


@app.get("/items/{item_id}")
async def get_item(item_id: str, q: str | None = None):
    if q:
        return {"item_id": item_id, "q": q}
    return {"item_id": item_id}


@app.post("/items", response_model=ItemOut)
async def create_item(item: Item) -> Any:
    return item
