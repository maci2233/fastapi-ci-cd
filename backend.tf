terraform {
        
    backend "s3" {
        bucket = "maci-fast-api-configs"
        key    = "tf.tfstate"
        region = "us-east-2"
    }
}